#!/bin/bash

#ホームディレクトリ
homedir="/home/$USER"

echo
echo "Ubuntu環境構築スクリプト"
echo
echo
echo "ubuntuの初期設定をまだしてないですか?(y/n)"
read ubuntu
echo "GoogleChromeをインストールしますか?(y/n)"
read goolgechorome
echo "Slackをインストールしますか?(y/n)"
read slack
echo "python-pipをインストールしますか?(y/n)"
read pythonpip
echo "VisualStudioCodeをインストールしますか?(y/n)"
read vscode
echo "Atomをインストールしますか?(y/n)"
read atom
echo "Pycharmをインストールしますか？(y/n)"
read pycharm
echo "Githubをインストールしますか？(y/n)"
read github

clear
echo
echo "ubuntu: "$ubuntu
echo "GoogleChrome: "$goolgechorome
echo "Slack: "$slack
echo "pythonpip: "$pythonpip
echo "VisualStudioCode: "$vscode
echo "Atom: "$atom
echo "Pycharm: "$pycharm
echo "github: "$github

echo
echo "これで大丈夫ですか？(y/n)"
read ok

if [ $ok = 'y' ]; then

	if [ $ubuntu = 'y' ]; then

		#ディレクトリ英語化
		cd .config
		sed -i s/'デスクトップ'/'Desktop'/g user-dirs.dirs
		sed -i s/'ダウンロード'/'Downloads'/g user-dirs.dirs
		sed -i s/'テンプレート'/'Temp'/g user-dirs.dirs
		sed -i s/'公開'/'Public'/g user-dirs.dirs
		sed -i s/'ドキュメント'/'Documents'/g user-dirs.dirs
		sed -i s/'ミュージック'/'Music'/g user-dirs.dirs
		sed -i s/'ピクチャ'/'Pictures'/g user-dirs.dirs
		sed -i s/'ビデオ'/'Videos'/g user-dirs.dirs
		cd ~/
		mv $HOME/デスクトップ $HOME/Desktop
		mv $HOME/ダウンロード $HOME/Downloads
		mv $HOME/テンプレート $HOME/Temp
		mv $HOME/公開 $HOME/Public
		mv $HOME/ドキュメント $HOME/Documents
		mv $HOME/ミュージック $HOME/Music
		mv $HOME/ピクチャ $HOME/Pictures
		mv $HOME/ビデオ $HOME/Videos

		#初期エラー対処
		sudo killall -KILL apt.systemd.daily
		sudo mv /etc/apt/apt.conf.d/50appstream /etc/apt/apt.conf.d/50appstream.disable
		sudo apt update -y
		sudo apt upgrade -y
		sudo mv /etc/apt/apt.conf.d/50appstream.disable /etc/apt/apt.conf.d/50appstream
		sudo apt update -y
		sudo timedatectl set-local-rtc true
		sudo apt-get -f intsall

	fi

	#goolgechorome
	if [ $goolgechorome = 'y' ]; then

		echo
		#echo "ブラウザから.debをダウンロードしてください。場所は変更しなくても大丈夫です。"
		echo
		#firefox https://www.google.com/chrome/browser/desktop/index.html?brand=CHBD&ds_kid=43700010867180244&gclid=EAIaIQobChMI792Bq5OG2AIVlgoqCh1K9AqbEAAYASAAEgLrVvD_BwE&gclsrc=aw.ds&dclid=CN7v8quThtgCFcM4lgodb3IPbA
		echo
		#echo "完了したら何か入力してください。"
		#read wait
		cd ~/Downloads/
		wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
		sudo apt-get install libnss3
		sudo dpkg -i google-chrome-stable_current_amd64.deb
		cd

	fi

	#スラック
	if [ $slack = 'y' ]; then
		
		wget https://downloads.slack-edge.com/linux_releases/slack-desktop-4.0.2-amd64.deb
		mv slack-desktop-4.0.2-amd64.deb ~/Downloads/
		cd ~/Downloads/
		sudo apt-get install libappindicator1
		sudo dpkg -i slack-desktop-4.0.2-amd64.deb
		cd
	fi

    #python-pip
    if [ $pythonpip = 'y' ]; then
		
		sudo apt install python-pip
		cd
	fi

	#VScode
	if [ $vscode = 'y' ]; then

		cd ~/Downloads/
		wget https://go.microsoft.com/fwlink/?LinkID=760868
		sudo dpkg -i `ls | grep "LinkID"`
		cd
		
	fi 

	#atom
	if [ $atom = 'y' ]; then
		
		echo
		echo "ブラウザから.debをダウンロードしてください。場所は変更しなくても大丈夫です。"
		echo
		firefox https://atom.io/
		echo
		echo "完了したら何か入力してください。"
		read wait
		cd ~/Downloads/
		sudo dpkg -i atom-amd64.deb
		cd
		
	fi
	
	#pycharm 
	if [ $pycharm = 'y' ]; then

		cd ~/Downloads/ 
		wget https://download.jetbrains.com/python/pycharm-community-2018.3.2.tar.gz
		tar -zxvf pycharm-community-2018.3.2.tar.gz
		mv pycharm-community-2018.3.2 ~/pycharm
		
		PYCHARM=`find ~/ -type d -name ".*" -prune -o -type f -print | grep "pycharm.sh" | sed 's@/bin/pycharm.sh@@g'`

		wget http://ftp.jaist.ac.jp/pub/mergedoc/pleiades/build/stable/pleiades.zip
		unzip pleiades.zip -d $PYCHARM
		echo -Xverify:none >> $PYCHARM/bin/pycharm64.vmoptions
		echo -javaagent:$PYCHARM/plugins/jp.sourceforge.mergedoc.pleiades/pleiades.jar >> $PYCHARM/bin/pycharm64.vmoptions
		echo -Xverify:none >> $PYCHARM/bin/pycharm.vmoptions
		echo -javaagent:$PYCHARM/plugins/jp.sourceforge.mergedoc.pleiades/pleiades.jar >> $PYCHARM/bin/pycharm.vmoptions

		# pycharmのアイコン
    	cd
		cd .local/share/applications/
		touch pycharm.desktop
		echo "[Desktop Entry]" > pycharm.desktop
		echo "Version=2018.1.2" >> pycharm.desktop
		echo "Type=Application" >> pycharm.desktop
		echo "Name=Pycharm" >> pycharm.desktop
		echo "Icon=/home/$USER/pycharm/bin/pycharm.png" >> pycharm.desktop
		echo "Exec=gksudo -S -k -u $USER /home/$USER/pycharm/bin/pycharm.sh" >> pycharm.desktop
		echo "Comment=pycharm!" >> pycharm.desktop
		echo "Categories=Development;IDE;" >> pycharm.desktop
		echo "Terminal=false" >> pycharm.desktop
		echo "StartupWMClass=Pycharmapp" >> pycharm.desktop
		cd
	fi

	#Github
	if [ $github = 'y' ]; then

		sudo apt-get install git
		cd
		
	fi

	echo
	echo
	echo "環境構築が完了しました。\n"
	echo "\n-------------------------Githubとアカウントを連携したい場合-------------------------\n"
	echo "git config --global user.name [アカウントのユーザ名]"
	echo "git config --global user.email [アカウントに繋げたメールアドレス]"
	echo "\n------------------------------------------------------------------------------------\n"

fi

exit 1