#!/bin/bash
#作製　hirose

echo
echo "ROSの追加のセットアップを開始しますか？(y/n)"
read ok

if [ $ok = 'y' ]; then
    
    #ROSの基本セットをまとめてインストール
    sudo apt-get install ros-kinetic-desktop-full
    sudo apt-get install python-rosinstall


    echo
    echo "環境構築が完了しました。"
    echo

fi

exit 1
