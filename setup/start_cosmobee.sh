#!/bin/bash

cd $HOME/freeflyer_build/native
source devel/setup.bash
cd
roslaunch astrobee sim.launch dds:=false robot:=sim_pub rviz:=true

exit 1