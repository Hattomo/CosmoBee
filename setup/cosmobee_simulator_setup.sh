#!/bin/bash
#作製　hirose

echo
echo "astrobeeのセットアップを開始しますか？(y/n)"
read ok

if [ $ok = 'y' ]; then

	
	sudo apt-get install git

	sudo apt-get install build-essential git
	export SOURCE_PATH=$HOME/freeflyer
	git clone https://github.com/nasa/astrobee.git $SOURCE_PATH
	export ANDROID_PATH="${SOURCE_PATH}_android"
	git clone https://github.com/nasa/astrobee_android.git $ANDROID_PATH
	
	sudo apt-get update
	sudo apt-get upgrade
	
	cd
	cd $SOURCE_PATH
	
	cd scripts/setup
	./add_ros_repository.sh
	sudo apt-get update
	cd debians
	./build_install_debians.sh
	cd ../
	./install_desktop_16_04_packages.sh
	sudo rosdep init
	rosdep update
	cd

	cd $SOURCE_PATH
	./scripts/configure.sh -l -F -D

	export BUILD_PATH=$HOME/freeflyer_build/native
	#デフォルトと違う場所にインストールしたい場合
	#export INSTALL_PATH=$HOME/freeflyer_install/native
	#./scripts/configure.sh -l -F -D -p $INSTALL_PATH -b $BUILD_PATH
	
	cd

	cd $BUILD_PATH
	make -j2
	cd

	echo
	echo "環境構築が完了しました。"
	echo
	
fi

exit 1
